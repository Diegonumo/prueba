# jan/02/1970 00:03:03 by RouterOS 6.43.8
# software id = 3SGT-LYR5
#
# model = RouterBOARD 941-2nD
# serial number = 7DE60710309A
/interface bridge
add name=bridge_ISP
/interface ethernet
set [ find default-name=ether1 ] comment=WAN
set [ find default-name=ether2 ] comment=LAN
set [ find default-name=ether3 ] comment=LAN
set [ find default-name=ether4 ] comment=LAN
/interface vlan
add interface=ether1 name=vlan6 vlan-id=6
/interface pppoe-client
add add-default-route=yes disabled=no interface=vlan6 name=pppoe-out1 \
    password=adslppp user=adslppp@telefonicanetpa
/interface wireless security-profiles
set [ find default=yes ] supplicant-identity=MikroTik
add authentication-types=wpa2-psk eap-methods="" mode=dynamic-keys name=\
    Red_N��ez-Moreno supplicant-identity="" wpa2-pre-shared-key=jutesepa
/interface wireless
set [ find default-name=wlan1 ] disabled=no mode=ap-bridge security-profile=\
    Red_N��ez-Moreno ssid=Redwificasa
/ip pool
add name=dhcp_pool0 ranges=192.168.1.163-192.168.1.199
/ip dhcp-server
add address-pool=dhcp_pool0 disabled=no interface=bridge_ISP name=dhcp1
/interface bridge port
add bridge=bridge_ISP interface=ether2
add bridge=bridge_ISP interface=ether4
add bridge=bridge_ISP interface=wlan1
/ip address
add address=192.168.1.1/24 interface=bridge_ISP network=192.168.1.0
add address=192.168.1.200/24 interface=ether3 network=192.168.1.0
/ip dhcp-server network
add address=192.168.1.0/24 dns-server=8.8.8.8,8.8.4.4 gateway=192.168.1.1
/ip firewall nat
# no interface
add action=masquerade chain=srcnat out-interface=pppoe-out1
